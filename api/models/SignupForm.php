<?php
namespace api\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $email;
    public $password;
    public $firstname;
    public $lastname;
    public $phone;
    public $error;


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email', 'firstname', 'lastname', 'phone'], 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            [['email', 'firstname', 'lastname', 'phone'], 'string', 'max' => 191],
            [['phone'], 'string', 'max' => 45],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            [['firstname', 'lastname', 'phone', 'password', 'email'], 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if (!$this->validate()) {
            $this->error = $this->getFirstErrors();
            return null;
        }

        $user = new User();
        $user->attributes = $this->attributes;
        $user->iso2 = "lb";
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if (!$user->save()) {
            \Yii::error(var_export($user->getErrors(), true));
            $this->error = $user->getFirstErrors();
            return false;
        }

        return $user;
    }
}
