<?php

namespace api\components;

use Yii;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

class CommonController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors = ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization', 'content-type'],
                    'Access-Control-Allow-Credentials' => true,
                ],
            ],
        ], $behaviors);

        return $behaviors;
    }

    public function actionError(){

    }

    public function beforeAction($action) {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Authorization, X-Accept-Charset, X-Accept, Accept, Origin, x-requested-with, Content-Type");
        if (($_SERVER['REQUEST_METHOD'] === 'OPTIONS')) {
            Yii::$app->end();
        }
        return parent::beforeAction($action);
    }

}
