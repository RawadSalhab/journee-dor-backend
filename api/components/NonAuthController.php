<?php

namespace api\components;

use Yii;
use yii\filters\AccessControl;

class NonAuthController extends CommonController {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                ],
            ],
        ];
        return $behaviors;
    }

    public function login() {
        $authHeader = Yii::$app->getRequest()->getHeaders()->get('Authorization');
        if ($authHeader !== null && preg_match("/^Bearer\\s+(.*?)$/", $authHeader, $matches) && $identity = Yii::$app->getUser()->loginByAccessToken($matches[1])) {
            return true;
        }
        return false;
    }

}
