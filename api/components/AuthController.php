<?php

namespace api\components;

use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;

class AuthController extends CommonController {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
//            'except' => $this->noAuthActions,
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

}
