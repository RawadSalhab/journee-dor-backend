<?php

namespace api\controllers;

use Yii;
use api\models\SignupForm;
use api\models\LoginForm;
use yii\web\NotFoundHttpException;
use api\components\NonAuthController;
use common\models\User;

/**
 * Site controller
 */
class UserController extends NonAuthController {

    public function actionLogin() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            if (Yii::$app->user->identity->verified == User::VERIFIED_NOT) {
                $response = Yii::$app->response;
//                $response->statusCode = 405;
//                $response->data = ['phone' => Yii::$app->user->identity->phone, 'access_token' => Yii::$app->user->identity->getAuthKey()];
                $response->data = ['access_token' => Yii::$app->user->identity->getAuthKey()];
                $response->send();
                Yii::$app->end();
            }
            return ['access_token' => Yii::$app->user->identity->getAuthKey()];
        } else {
            $model->validate();
            return $model;
        }
    }

//    public function actionValidatenewuser() {
//        $model = new SignupForm();
//        $model->scenario = 'validate';
//        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
//            if ($model->validate()) {
//                return;
//            }
//            return $model;
//        }
//    }

//    public function actionVerifyphone() {
//        if (!$this->login()) {
//            throw new NotFoundHttpException();
//        }
//        $POST = Yii::$app->getRequest()->getBodyParams();
//        if (isset($POST['code'])) {
//            $user = Yii::$app->user->identity;
//            if ($user->verifyCheck($POST['code'])) {
//                return ['access_token' => $user->getAuthKey()];
//            }
//            $error = "Invalid or expired code! Please try again.";
//        }
//        Yii::$app->response->statusCode = 422;
//        return ['error' => isset($error) ? $error : "Something wrong just happened, please try again later or contact our support team."];
//    }

//    public function actionChangephone() {
//        if (!$this->login()) {
//            throw new NotFoundHttpException();
//        }
//        $POST = Yii::$app->getRequest()->getBodyParams();
//        if (isset($POST['phone']) && isset($POST['iso2'])) {
//            $user = Yii::$app->user->identity;
//            $user->phone = $POST['phone'];
//            $user->iso2 = $POST['iso2'];
//            $user->verified = User::VERIFIED_NOT;
//            if ($user->reStartVerify()) {
//                if ($user->save()) {
//                    return ['phone' => $user->phone];
//                }
//            }
//            $error = "A recent verification request has already been started for this phone number. Please try again in a few minutes.";
//        }
//        Yii::$app->response->statusCode = 422;
//        return ['error' => isset($error) ? $error : "Something wrong just happened, please try again later or contact our support team."];
//    }

    public function actionSignup() {
        $model = new SignupForm();
        $POST = Yii::$app->getRequest()->getBodyParams();
        if ($model->load($POST, '')) {
            if ($user = $model->signup()) {
//                Yii::$app->response->statusCode = 405;
                return ['phone' => $user->phone, 'access_token' => $user->getAuthKey()];
            }
            if (isset($model->error) && !empty($model->error)) {
                $error = $model->error;
            }
        }
        Yii::$app->response->statusCode = 422;
        return ['error' => isset($error) ? $error : "Something wrong just happened, please try again later or contact our support team."];
    }
//
//    public function actionRestartverify() {
//        if (!$this->login()) {
//            throw new NotFoundHttpException();
//        }
//        if (Yii::$app->user->identity->reStartVerify()) {
//            return;
//        }
//        $error = "A recent verification request has already been started. Please try again in a few minutes.";
//        Yii::$app->response->statusCode = 422;
//        return ['error' => isset($error) ? $error : "Something wrong just happened, please try again later or contact our support team."];
//    }
//
//    public function actionResetpassword() {
//        $POST = Yii::$app->getRequest()->getBodyParams();
//        if (!$reset = Resetpassword::initiateReset($POST['email'])) {
//            return ['error' => 'Unable to initiate reset request'];
//        }
//        if ($reset->hasErrors()) {
//            Yii::$app->response->statusCode = 422;
//            return ['error' => $reset->getFirstErrors()];
//        }
//        return ['id' => $reset->id, 'phone' => $reset->phone];
//    }
//
//    public function actionVerifyresetcode() {
//        $POST = Yii::$app->getRequest()->getBodyParams();
//        if ($reset = Resetpassword::findOne($POST['id'])) {
//            if ($reset->validateCode($POST['code'])) {
//                return true;
//            }
//        }
//        Yii::$app->response->statusCode = 422;
//        $error = 'Invalid code...';
//        $tooMany = false;
//        if ($reset->hasErrors()) {
//            $tooMany = true;
//            $error = $reset->getFirstError('user_id');
//        }
//        return ['error' => $error, 'toomany' => $tooMany];
//    }
//
//    public function actionResetchangepassword() {
//        $POST = Yii::$app->getRequest()->getBodyParams();
//        if ($reset = Resetpassword::findOne($POST['id'])) {
//            if (strlen($POST['password']) < 6) {
//                $error = 'Password should be at least 6 characters long';
//                Yii::$app->response->statusCode = 422;
//                return ['error' => $error];
//            }
//            if ($reset->changePassword($POST['code'], $POST['password'])) {
//                return;
//            }
//
//            Yii::$app->response->statusCode = 422;
//            return ['error' => 'This code is expired or have already been used.'];
//        }
//    }

}
