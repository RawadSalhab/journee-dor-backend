<?php

namespace api\controllers;

use common\models\Page;
use Yii;
use api\models\SignupForm;
use yii\web\NotFoundHttpException;
use api\components\NonAuthController;
use common\models\User;

class PagesController extends NonAuthController {
    public function actionHome() {
        if (!$home = Page::findActive()->andWhere(['type' => Page::TYPE_HOME])->one()) {
            throw new NotFoundHttpException();
        }
        return $home->apiarray;
    }

    public function actionPage() {
        $POST = Yii::$app->getRequest()->getBodyParams();
        if (!$page = Page::findOne($POST['id'])) {
            throw new NotFoundHttpException();
        }
        return $page->apiarray;
    }
}
