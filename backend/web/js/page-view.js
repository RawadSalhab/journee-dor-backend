$(document).ready(function () {

    var form = $('#new-page-form');

    $('#imageAreaModal').modal({
        show: false,
        backdrop: "static",
    });

    var currImage = {};
    var currSelection = {};

    $('#imageAreaModal').on('shown.bs.modal', function () {
        currSelection = $('#uploadModalPreview').imgAreaSelect({
            handles: true,
            x1: "0",
            y1: "0",
            x2: "638",
            y2: "638",
            imageHeight: currImage.height,
            imageWidth: currImage.width,
            minHeight: "638",
            minWidth: "638",
            maxWidth: "5120",
            parent: '.modal-content',
            persistent: true,
            instance: true,
        });
    });

    var p = $(".uploadPreviewClass");

    // prepare instant preview
    $(".uploadimg").on("change", function () {
        var inputField = $(this);

        // prepare HTML5 FileReader
        var oFReader = new FileReader();
        oFReader.readAsDataURL(inputField.get(0).files[0]);

        oFReader.onload = function (oFREvent) {
            var img = new Image();
            img.src = oFReader.result;
            img.onload = function () {
                if (this.width < 640 || this.height < 640) {
                    alert("Image should be at least 640 x 640!");
                    fileInputField.replaceWith(input.val("").clone(true));
                    return;
                }
                currImage.width = this.width;
                currImage.height = this.height;
                $("#uploadModalPreview").attr("src", oFREvent.target.result).fadeIn(function () {
                    $('#imageAreaModal').modal("show");
                });
            }
        };

    });

    $("#saveModal").on("click", function () {
        var sel = currSelection.getSelection();
        $("#picX").val(sel.x1);
        $("#picY").val(sel.y1);
        $("#picW").val(sel.width);
        $("#picH").val(sel.height);
        form.yiiActiveForm('submitForm');
        $('#imageAreaModal').modal("hide");
    });

    $(".cancelModal").on("click", function () {

    });

    var clearFileInputs = function () {
        $('.uploadimg').each(function (i, obj) {
            $(this).replaceWith($(this).val("").clone(true));
        });
    }

});