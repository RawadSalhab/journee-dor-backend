<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = $model->isNewRecord ? 'New Page' : $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['/pages/index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $this->title ?></h3>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(['id' => 'new-page-form']); ?>
            <div class="form-group">
                <?php if (!$model->isNewRecord) { ?>
                    <div class="center squareImgMaxHeight imgHover" id="menuimg">
                        <label for="image-upload">
                            <img src="<?= $model->src() ?>"/>
                            <span class="text-content"><span>Change Picture</span></span>
                        </label>
                        <?= $form->field($model, 'x', ['options' => ['class' => 'hide']])->hiddenInput(['id' => "picX"])->label(false); ?>
                        <?= $form->field($model, 'y', ['options' => ['class' => 'hide']])->hiddenInput(['id' => "picY"])->label(false); ?>
                        <?= $form->field($model, 'w', ['options' => ['class' => 'hide']])->hiddenInput(['id' => "picW"])->label(false); ?>
                        <?= $form->field($model, 'h', ['options' => ['class' => 'hide']])->hiddenInput(['id' => "picH"])->label(false); ?>
                        <?=
                        $form->field($model, 'pic')->fileInput([
                            'class' => 'uploadimg',
                            'id' => 'image-upload',
                            'accept' => 'image/png, image/jpeg'])->label(false);
                        ?>
                        <?php if ($model->hasPic() && !$model->isImage()) { ?>
                            <p class="center"><?= Html::a('Remove Picture', ['pages/remove-page-pic', 'id' => $model->id]) ?></p>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?= $form->field($model, 'title'); ?>
                <?php if ($model->isImage()) { ?>
                    <?= $form->field($model, 'description'); ?>
                <?php } ?>
                <?= $form->field($model, 'type')->dropDownList($model::listTypes()) ?>
                <?= $form->field($model, 'status')->dropDownList($model::listStatus()) ?>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php if (!$model->isNewRecord) { ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Child Pages</h3>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(['id' => 'sub-pages-form']); ?>
            <div class="form-group">
                <?= \kartik\select2\Select2::widget([
                    'name' => 'pages',
                    'value' => $model->getChildPagesList(),
                    'data' => \common\models\Page::listPages(),
                    'options' => ['multiple' => true, 'placeholder' => 'Select Pages'],
                    'maintainOrder' => true,
                ]);
                ?>
                <?= Html::input('hidden', 'childpages'); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php } ?>

<?php if (!$model->isNewRecord) { ?>
    <!-- Modal -->
    <div class="modal fade" id="imageAreaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close cancelModal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Crop Picture</h4>
                </div>
                <div class="modal-body">
                    <img class="img-responsive" id="uploadModalPreview"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default cancelModal" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="saveModal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php
if (!$model->isNewRecord) {
    $this->registerCssFile(Url::base() . "/lib/imgareaselect/dist/imgareaselect.css");
    $this->registerJsFile(
        Url::base() . "/lib/imgareaselect/dist/jquery.imgareaselect.js", ['depends' => ['\backend\assets\AppAsset']]
    );
    $this->registerJsFile(
        Url::base() . "/js/page-view.js", ['depends' => ['\backend\assets\AppAsset'], 'position' => \yii\web\View::POS_END]
    );
}