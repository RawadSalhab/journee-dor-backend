<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

$this->title = 'Pages';
?>
<div class="site-index">

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'attribute' => 'type',
                'value' => function ($data) {
                    return $data->type();
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => ['data' => [null => 'All'] + \common\models\Page::listTypes()],
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
        'responsive' => true,
        'hover' => true,
        'striped' => false,
        'pjax' => true,
        'panel' => [
            'type' => 'panel panel-primary',
            'heading' => '<h3 class="panel-title"> Pages</h3>',
        ],
        'toolbar' => [
            [
                'content' =>
                    Html::a('<i class="glyphicon glyphicon-plus"></i> New', Url::toRoute("/pages/view"), [
                        'type' => 'button',
                        'title' => 'New',
                        'class' => 'btn btn-success'
                    ]),
            ]
        ],
    ]);
    ?>
</div>
