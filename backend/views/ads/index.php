<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

$this->title = 'Ads';
?>
<div class="site-index">

    <?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'class' => '\kartik\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
        'responsive' => true,
        'hover' => true,
        'striped' => false,
        'pjax' => true,
        'panel' => [
            'type' => 'panel panel-primary',
            'heading' => '<h3 class="panel-title"> Ads</h3>',
        ],
        'toolbar' => [
            [
                'content' =>
                    Html::a('<i class="glyphicon glyphicon-plus"></i> New', Url::toRoute("/ads/view"), [
                        'type' => 'button',
                        'title' => 'New',
                        'class' => 'btn btn-success'
                    ]),
            ]
        ],
    ]);
    ?>
</div>
