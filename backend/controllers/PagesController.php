<?php
namespace backend\controllers;

use common\models\Page;
use common\models\PageSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;


class PagesController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id = null) {
        if (!$id || !$model = Page::findOne($id)) {
            $model = new Page();
        }
        $POST = Yii::$app->request->post();
        if ($model->load($POST)) {
            if (($model->pic = UploadedFile::getInstance($model, 'pic'))) {
                if ($model->save()) {
                    if ($model->uploadImg()) {
                        Yii::$app->session->setFlash('success', 'Picture added successfully!');
                        return $this->refresh();
                    }
                }
            }
            if ($model->isNewRecord && $model->save()) {
                Yii::$app->session->setFlash('success', 'New page added successfully!');
                return $this->redirect(['pages/index']);
            } else if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Page Edited successfully!');
            }
        }
        if (isset($POST['childpages'])) {
            $pages = isset($POST['pages']) ? $POST['pages'] : [];
            $model->setPages($pages);
            $model = Page::findOne($model->id);
            Yii::$app->session->setFlash('success', 'Child pages updated successfully!');
        }
        return $this->render('page', ['model' => $model]);
    }

    public function actionRemovePagePic($id) {
        $model = Page::findOne($id);
        if ($model->removePic()) {
            Yii::$app->session->setFlash('success', 'Page image removed!');
        } else {
            Yii::$app->session->setFlash('error', 'Unable to remove page image...');
        }
        return $this->redirect(['pages/view', 'id' => $model->id]);
    }


}
