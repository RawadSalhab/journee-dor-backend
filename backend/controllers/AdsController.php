<?php
namespace backend\controllers;

use common\models\Ad;
use common\models\AdSearch;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


class AdsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new AdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id = null) {
        if (!$id || !$model = Ad::findOne($id)) {
            $model = new Ad();
        }
        $POST = Yii::$app->request->post();
        if ($model->load($POST)) {
            if ($model->isNewRecord && $model->save()) {
                Yii::$app->session->setFlash('success', 'New Ad added successfully!');
                return $this->redirect(['ads/index']);
            } else if ($model->save()) {
                if (($model->pic = UploadedFile::getInstance($model, 'pic'))) {
//                    die();
                    $model->uploadImg();
                }
                Yii::$app->session->setFlash('success', 'Ad Edited successfully!');
            }
        }
        return $this->render('view', ['model' => $model]);
    }

}
