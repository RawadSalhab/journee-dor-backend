<?php

namespace common\components;

use Yii;
use Nexmo\Client;
use common\models\Verify;
use Nexmo\Exception;

class Nexmo {

    public $client;
    public $response;
    public $exceptionVar;

    public function __construct() {
//        $this->client = new Client("516e632a", "ae2b243b"); // rayan@voiplet.com
        $this->client = new Client("6be49646", "e3ee61c5ff227dd8"); // info@goemu.com
        $this->exceptionVar = false;
    }

    public function sendSms($to, $text) {
        $from = "Waitron";
        try {
            $response = $this->client->message->invoke($from, $to, 'text', $text);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $this->response = $response;
        foreach ($response['messages'] as $i => $m) {
            switch ($m['status']) {
                case '0':
                    return true;
                default:
                    return false;
            }
        }
    }

    public function sendVerify($to) {
        try {
            $response = $this->client->verify->invoke($to, "Waitron", "Waitron", 6);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'nexmo');
            $this->exceptionVar = true;
            return false;
        }
        $this->response = $response;
        if ($response['status'] == 0) {
            return true;
        }
        return false;
    }

    public function verifyCheck($requestId, $code) {
        try {
            $response = $this->client->verify->check->invoke($requestId, $code);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'nexmo');
            $this->exceptionVar = true;
            return false;
        }
        $this->response = $response;
        if ($response['status'] == 0) {
            return true;
        }
        return false;
    }

}
