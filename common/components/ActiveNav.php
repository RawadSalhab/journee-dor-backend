<?php

namespace common\components;

use Yii;

class ActiveNav {

    public static function check($keyword) {
        if (isset(Yii::$app->view->params['breadcrumbs']))
            return self::recursive_array_search($keyword, Yii::$app->view->params['breadcrumbs']);
        return false;
    }

    public static function recursive_array_search($needle, $haystack) {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR ( is_array($value) && self::recursive_array_search($needle, $value) !== false)) {
                return true;
            }
        }
        return false;
    }

}
