<?php

namespace common\components;

use Yii;

class UploadHelper {

    public static function randDir() {
        $len = rand(14, 18);
        $abc = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $defaultLength = 3;
        $len = max(min(intval($len), strlen($abc)), $defaultLength);
        return substr(str_shuffle($abc), 0, $len);
    }

}
