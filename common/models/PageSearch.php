<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ReservationSearch represents the model behind the search form about `common\models\currency\Currency`.
 */
class PageSearch extends Page {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Page::find();
//        $query->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

//        $dataProvider->setSort([
//            'attributes' => [
//                ''
//                'email',
//                'updated_at'
//            ]
//        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'LOWER(title)', strtolower($this->title)]);
        $query->andFilterWhere(['like', 'type', $this->type]);
//        $query->andFilterWhere(['like', 'LOWER(TYPE)', $this->type()]);
//        $query->andFilterWhere(['like', 'LOWER(email)', strtolower($this->email)]);

        return $dataProvider;
    }

}
