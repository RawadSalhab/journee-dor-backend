<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "parent_has_page".
 *
 * @property integer $parent_id
 * @property integer $page_id
 *
 * @property Page $page
 * @property Page $parent
 */
class ParentHasPage extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'parent_has_page';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['parent_id', 'page_id'], 'required'],
            [['parent_id', 'page_id'], 'integer'],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'parent_id' => 'Parent ID',
            'page_id' => 'Page ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage() {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Page::className(), ['id' => 'parent_id']);
    }
    
}
