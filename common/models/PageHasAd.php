<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_has_ad".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $ad_id
 *
 * @property Ad $ad
 * @property Page $page
 */
class PageHasAd extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'page_has_ad';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['page_id', 'ad_id'], 'required'],
            [['page_id', 'ad_id'], 'integer'],
            [['ad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ad::className(), 'targetAttribute' => ['ad_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'ad_id' => 'Ad ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAd() {
        return $this->hasOne(Ad::className(), ['id' => 'ad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage() {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}
