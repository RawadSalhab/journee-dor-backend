<?php

namespace common\models;

use common\components\UploadHelper;
use Yii;

/**
 * This is the model class for table "ad".
 *
 * @property integer $id
 * @property string $title
 * @property integer $type
 * @property integer $status
 * @property string $src
 *
 * @property PageHasAd[] $pageHasAds
 */
class Ad extends \yii\db\ActiveRecord {

    const TYPE_TOP_BANNER = 10;

    const STATUS_ACTIVE = 10;
    const STATUS_SUSPENDED = 20;
    const STATUS_DELETED = 30;

    public $pic;
    public $x;
    public $y;
    public $w;
    public $h;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ad';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['type', 'status'], 'integer'],
            [['title', 'src'], 'string', 'max' => 191],
            ['type', 'default', 'value' => self::TYPE_TOP_BANNER],
            ['type', 'in', 'range' => [self::TYPE_TOP_BANNER]],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_SUSPENDED, self::STATUS_DELETED]],
            [['x', 'y', 'w', 'h'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'type' => 'Type',
            'status' => 'Status',
            'src' => 'Src',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageHasAds() {
        return $this->hasMany(PageHasAd::className(), ['ad_id' => 'id']);
    }

    public function uploadImg() {
        if ($this->validate()) {
            if (!isset($this->w) || !isset($this->h) || !isset($this->x) || !isset($this->y)) {
                return false;
            }
            $imageInfo = getimagesize($this->pic->tempName);
            list($width, $height) = $imageInfo;

            $imageInfo = getimagesize($this->pic->tempName);
            switch ($imageInfo[2]) {
                case IMAGETYPE_JPEG :
                    $src = imagecreatefromjpeg($this->pic->tempName);
                    break;
                case IMAGETYPE_PNG :
                    $src = imagecreatefrompng($this->pic->tempName);
                    break;
                default :
                    Yii::$app->session->setFlash('error', 'Invalid image format...');
                    return false;
            }

            $rand1 = UploadHelper::randDir();
            $uniqueid = uniqid();
            $imagedir = Yii::getAlias('@uploadsPath') . DIRECTORY_SEPARATOR . $rand1;
            $directory = $imagedir . DIRECTORY_SEPARATOR . "image_" . $uniqueid . '.jpg';

            $directorydb = '/' . $rand1 . "/image_" . $uniqueid . '.jpg';

            if (!file_exists($imagedir)) {
                mkdir($imagedir, 0777, true);
            }

            //Cropped Image
            if ($this->w > 1080) {
                $destW = 1080;
            } else {
                $destW = $this->w;
            }
            $destH = $destW / 4;

            $croppedImage = imagecreatetruecolor($destW, $destH);
            imagecopyresampled($croppedImage, $src, 0, 0, $this->x, $this->y, $destW, $destH, $this->w, $this->h);

            imagejpeg($croppedImage, $directory);
            imagedestroy($croppedImage);

            $this->src = $directorydb;
            if ($this->save()) {
                return true;
            }
        } else {
            Yii::$app->session->setFlash('error', 'Error Saving image...');
            return false;
        }
    }

    public function coverSrc($canNull = false) {
        if (!$this->src) {
            if ($canNull) {
                return null;
            }
            return 'http://placehold.it/800x200';
        }
        return Yii::getAlias("@uploads") . $this->src;
    }
}
