<?php

use yii\db\Schema;
use yii\db\Migration;

class m170206_174618_session extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%session}}',
            [
                'id'=> $this->char(40)->notNull(),
                'expire'=> $this->integer(11)->null()->defaultValue(null),
                'data'=> $this->binary()->null()->defaultValue(null),
            ],$tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%session}}');
    }
}
