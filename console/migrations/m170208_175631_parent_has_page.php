<?php

use yii\db\Schema;
use yii\db\Migration;

class m170208_175631_parent_has_page extends Migration {

    public function init() {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp() {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%parent_has_page}}',
            [
                'parent_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
                'page_id'=>Schema::TYPE_INTEGER . '(11) NOT NULL',
            ],$tableOptions
        );
        $this->createIndex('fk_page_has_page_page1_idx', '{{%parent_has_page}}', 'page_id', false);
        $this->createIndex('fk_page_has_page_page_idx', '{{%parent_has_page}}', 'parent_id', false);
    }

    public function safeDown() {
        $this->dropIndex('fk_page_has_page_page1_idx', '{{%parent_has_page}}');
        $this->dropIndex('fk_page_has_page_page_idx', '{{%parent_has_page}}');
        $this->dropTable('{{%parent_has_page}}');
    }
}
