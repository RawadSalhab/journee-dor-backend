<?php

use yii\db\Schema;
use yii\db\Migration;

class m170423_162436_page_description extends Migration {
    public function safeUp() {
        $this->addColumn('{{%page}}', 'description', Schema::TYPE_TEXT . ' DEFAULT NULL AFTER `title`');
    }

    public function safeDown() {
        $this->dropColumn('{{%page}}', 'description');
    }
}
