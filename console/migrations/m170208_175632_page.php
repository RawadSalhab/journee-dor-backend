<?php

use yii\db\Schema;
use yii\db\Migration;

class m170208_175632_page extends Migration {

    public function init() {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp() {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%page}}',
            [
                'id' => $this->primaryKey(11),
                'title' => $this->string(191)->notNull(),
                'type' => $this->smallInteger(6)->notNull(),
                'status' => $this->smallInteger(6)->notNull(),
            ], $tableOptions
        );
        $this->createIndex('id_UNIQUE', '{{%page}}', 'id', true);
    }

    public function safeDown() {
        $this->dropIndex('id_UNIQUE', '{{%page}}');
        $this->dropTable('{{%page}}');
    }
}
