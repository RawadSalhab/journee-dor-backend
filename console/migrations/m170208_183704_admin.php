<?php

use yii\db\Schema;
use yii\db\Migration;

class m170208_183704_admin extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%admin}}',
            [
                'id'=> $this->primaryKey(11),
                'email'=> $this->string(191)->notNull(),
                'auth_key'=> $this->string(32)->notNull(),
                'password_hash'=> $this->string(191)->notNull(),
                'password_reset_token'=> $this->string(45)->null()->defaultValue(null),
                'status'=> $this->smallInteger(6)->notNull()->defaultValue(10),
                'created_at'=> $this->integer(11)->notNull(),
                'updated_at'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('email','{{%admin}}','email',true);
    }

    public function safeDown()
    {
        $this->dropIndex('email', '{{%admin}}');
        $this->dropTable('{{%admin}}');
    }
}
