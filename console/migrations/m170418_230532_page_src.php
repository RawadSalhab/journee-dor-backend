<?php

use yii\db\Schema;
use yii\db\Migration;

class m170418_230532_page_src extends Migration {
    public function safeUp() {
        $this->addColumn('{{%page}}', 'src', Schema::TYPE_STRING . '(191) DEFAULT NULL AFTER `title`');
    }

    public function safeDown() {
        $this->dropColumn('{{%page}}', 'src');
    }
}