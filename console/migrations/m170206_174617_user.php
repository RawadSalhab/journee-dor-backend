<?php

use yii\db\Schema;
use yii\db\Migration;

class m170206_174617_user extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%user}}',
            [
                'id'=> $this->primaryKey(11),
                'auth_key'=> $this->string(32)->notNull(),
                'password_hash'=> $this->string(191)->notNull(),
                'password_reset_token'=> $this->string(191)->null()->defaultValue(null),
                'email'=> $this->string(191)->notNull(),
                'status'=> $this->smallInteger(6)->notNull()->defaultValue(10),
                'firstname'=> $this->string(191)->notNull(),
                'lastname'=> $this->string(191)->notNull(),
                'phone'=> $this->string(45)->notNull(),
                'iso2'=> $this->string(45)->notNull(),
                'verified'=> $this->smallInteger(6)->notNull(),
                'nexmo_event'=> $this->string(45)->null()->defaultValue(null),
                'created_at'=> $this->integer(11)->notNull(),
                'updated_at'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('email','{{%user}}','email',true);
        $this->createIndex('password_reset_token','{{%user}}','password_reset_token',true);
    }

    public function safeDown()
    {
        $this->dropIndex('email', '{{%user}}');
        $this->dropIndex('password_reset_token', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}
