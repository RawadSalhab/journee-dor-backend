<?php

use yii\db\Schema;
use yii\db\Migration;

class m170215_234033_parent_has_page_primary_key extends Migration {
    public function safeUp() {
        $this->addColumn('{{%parent_has_page}}', 'id', Schema::TYPE_PK);
        $this->createIndex('id_UNIQUE', '{{%parent_has_page}}', 'id', 1);
    }

    public function safeDown() {
        $this->dropColumn('{{%parent_has_page}}', 'id');
    }
}
