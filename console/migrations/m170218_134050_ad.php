<?php

use yii\db\Schema;
use yii\db\Migration;

class m170218_134050_ad extends Migration {
    public function init() {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp() {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%ad}}',
            [
                'id' => Schema::TYPE_PK,
                'title' => $this->string(191)->notNull(),
                'type' => $this->smallInteger(6)->notNull(),
                'status' => $this->smallInteger(6)->notNull(),
                'src' => $this->string(191),
            ], $tableOptions
        );
        $this->createIndex('id_UNIQUE', '{{%ad}}', 'id', true);

        $this->createTable(
            '{{%page_has_ad}}',
            [
                'id' => Schema::TYPE_PK,
                'page_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
                'ad_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            ], $tableOptions
        );
        $this->createIndex('fk_page_has_ad_idx', '{{%page_has_ad}}', 'page_id', false);
        $this->createIndex('fk_page_has_ad1_idx', '{{%page_has_ad}}', 'ad_id', false);
        $this->addForeignKey('fk_page_has_ad_page_id', '{{%page_has_ad}}', 'page_id', 'page',
            'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_page_has_ad_ad_id', '{{%page_has_ad}}', 'ad_id', 'ad',
            'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_page_has_ad_page_id', '{{%page_has_ad}}');
        $this->dropForeignKey('fk_page_has_ad_ad_id', '{{%page_has_ad}}');
        $this->dropIndex('fk_page_has_ad_idx', '{{%page_has_ad}}');
        $this->dropIndex('fk_page_has_ad1_idx', '{{%page_has_ad}}');
        $this->dropIndex('id_UNIQUE', '{{%ad}}');
        $this->dropTable('{{%page_has_ad}}');
        $this->dropTable('{{%ad}}');
    }
}
