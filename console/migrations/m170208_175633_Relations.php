<?php

use yii\db\Schema;
use yii\db\Migration;

class m170208_175633_Relations extends Migration {

    public function init() {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp() {
        $this->addForeignKey('fk_parent_has_page_parent_id', '{{%parent_has_page}}', 'parent_id', 'page',
            'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_parent_has_page_page_id', '{{%parent_has_page}}', 'page_id', 'page',
            'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_parent_has_page_parent_id', '{{%parent_has_page}}');
        $this->dropForeignKey('fk_parent_has_page_page_id', '{{%parent_has_page}}');
    }
}
